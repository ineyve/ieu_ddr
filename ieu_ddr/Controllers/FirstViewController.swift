//
//  FirstViewController.swift
//  ieu_ddr
//
//  Created by formando on 20/09/2018.
//  Copyright © 2018 ipleiria. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    
    @IBOutlet weak var MyLabel: UILabel!
    
    
    @IBOutlet weak var Nome: UITextField!
    
    @IBOutlet weak var Email: UITextField!
    
    @IBOutlet weak var Idade: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        MyLabel.text = "Olá"
        
//        let i = 10
//        print ("i: \(i)")
//
//        var j:Int?
//        print ("j: \(j)")
//        
//        j=10
//        print ("j: \(j)")
//
    }
    @IBAction func ButtonAction(_ sender: UIButton) {
        MyLabel.text = "Clicked"
    }
    
    @IBAction func Guardar(_ sender: UIButton) {
        var pessoa = Pessoa(nome: Nome.text!, email: Email.text!)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate;
        
        appDelegate.pessoas.append(pessoa)
        
        for p in appDelegate.pessoas {
            print("nome = "+p.nome+" email = "+p.email+" idade = "+p.idade)
        }
    }
}

