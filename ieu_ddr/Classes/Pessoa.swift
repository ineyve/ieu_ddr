//
//  Pessoa.swift
//  ieu_ddr
//
//  Created by formando on 21/09/2018.
//  Copyright © 2018 ipleiria. All rights reserved.
//

import Foundation

class Pessoa {
    var nome:String
    var email:String
    var idade:Int = 19
    
    init(nome:String, email:String) {
        self.nome = nome
        self.email = email
    }
}
